﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RocketCore.Gateways.TCP.Messages
{
    class BalanceMsg : IJsonSerializable
    {
        private int user_id;
        private Account account;
        private DateTime dt_made;

        internal BalanceMsg(int user_id, Account account, DateTime dt_made) //конструктор сообщения
        {
            this.user_id = user_id;
            this.account = account;
            this.dt_made = dt_made;
        }

        public string Serialize()
        {
            return JsonManager.FormTechJson((int)MessageTypes.NewBalance, user_id, account, dt_made);
        }
    }
}
