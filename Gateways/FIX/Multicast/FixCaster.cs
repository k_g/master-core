﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using QuickFix;
using QuickFix.Fields;

namespace RocketCore
{
    class FixCaster
    {
        private static UdpClient client = new UdpClient();
        private static IPEndPoint orders_endpoint = new IPEndPoint(IPAddress.Broadcast, 15001);
        private static IPEndPoint trades_endpoint = new IPEndPoint(IPAddress.Broadcast, 15005);
        private static int orders_msg_seq_num;
        private static int trades_msg_seq_num;

        internal FixCaster()
        {
            orders_msg_seq_num = 0;
            trades_msg_seq_num = 0;

            Thread fix_orders_caster_thread = new Thread(new ThreadStart(HandleFixOrdersCasterThread));
            fix_orders_caster_thread.Start();

            Console.WriteLine("FIX: multicast orders thread started");

            Thread fix_trades_caster_thread = new Thread(new ThreadStart(HandleFixTradesCasterThread));
            fix_trades_caster_thread.Start();

            Console.WriteLine("FIX: multicast trades thread started");
        }

        internal void ResetMsgSeqNums()
        {
            orders_msg_seq_num = 0;
            trades_msg_seq_num = 0;
        }

        private void HandleFixOrdersCasterThread()
        {
            while (true)
            {
                //отправляем FIX-сообщения из очередей
                Message msg;
                if (Queues.fix_orders_queue.TryPeek(out msg))
                {
                    try
                    {
                        orders_msg_seq_num++;
                        msg.SetField(new IntField(34, orders_msg_seq_num)); //header field (MsgSeqNum)

                        byte[] bytes = Encoding.ASCII.GetBytes(msg.ToString());
                        client.Send(bytes, bytes.Length, orders_endpoint);

                        Queues.fix_orders_queue.TryDequeue(out msg);

                        //Console.WriteLine("FIX: order #" + orders_msg_seq_num.ToString() + " multicasted");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
        }

        private void HandleFixTradesCasterThread()
        {
            while (true)
            {
                //отправляем FIX-сообщения из очередей
                Message msg;
                if (Queues.fix_trades_queue.TryPeek(out msg))
                {
                    try
                    {
                        trades_msg_seq_num++;
                        msg.SetField(new IntField(34, trades_msg_seq_num)); //header field (MsgSeqNum)

                        byte[] bytes = Encoding.ASCII.GetBytes(msg.ToString());
                        client.Send(bytes, bytes.Length, trades_endpoint);

                        Queues.fix_trades_queue.TryDequeue(out msg);

                        //Console.WriteLine("FIX: trade #" + trades_msg_seq_num.ToString() + " multicasted");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
        }

    }
}
